#!/bin/bash

cd /koha

if [ -z "${VERSION}" ]; then
	VERSION_OPTIONS="--autoversion"
else
	VERSION_OPTIONS="-v ${VERSION} --noautoversion"
fi

./debian/build-git-snapshot \
	--basetgz base \
	-r /debs \
	${VERSION_OPTIONS} -d
