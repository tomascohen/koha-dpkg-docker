FROM debian:stretch-slim

LABEL maintainer="tomascohen@theke.io"

# Valid: master, major.minor (19.11, etc)
ARG BRANCH

ENV PATH /usr/bin:/bin:/usr/sbin:/sbin
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update \
    && apt-get -y --allow-unauthenticated install \
      devscripts \
      pbuilder \
      dh-make \
      fakeroot \
      debian-archive-keyring \
      gnupg2 \
      build-essential \
      wget \
      git \
      libmodern-perl-perl \
   && rm -rf /var/cache/apt/archives/* \
   && rm -rf /var/lib/api/lists/*

RUN mkdir -p /var/cache/pbuilder/
COPY base.tgz /var/cache/pbuilder/base.tgz

VOLUME /koha
VOLUME /debs

ENV PERL5LIB=/koha

ADD build.sh /build.sh

CMD /build.sh
