# koha-dpkg-docker - generate Koha packages inside a docker container

This project provides a way to generate Debian (.deb) packages for the
Koha project, without the need to have any complicated setups.

This is specially useful for automated builds in CI/CD workflows.

## Usage

In order to generate Debian (.deb) packages for the Koha project you need to have
* a git clone of the Koha source (_/path/to/your/clone_ in the sample commands)
* a valid target directory (_/dest/path/for/debs_, it needs to exist and have the right permissions)

Once you have that, you need to checkout the branch you want to build packages for. If it was the current HEAD of the master branch you should:

```
  $ cd /path/to/your/clone
  $ git fetch ; git checkout master -b my_master_build
```

Last step, build the packages:

```
  $ docker run \
      --volume=/path/to/your/clone:/koha \
      --volume=/dest/path/for/debs:/debs \
      --privileged \
      --env VERSION=19.12.00.061 \
      koha/koha-dpkg:master
```

Notice we used koha/koha-dpkg:__master__. If you want to build __19.11__ or __19.05__ packages, you need to pick those instead of __master__. For example:


```
  $ docker run \
      --volume=/path/to/your/clone:/koha \
      --volume=/dest/path/for/debs:/debs \
      --privileged \
      --env VERSION=19.11.08~patched1 \
      koha/koha-dpkg:19.05
```

You will find the built packages in the __debs__ dir (__/dest/path/for/debs__ in this example).


## Building the Docker images

If you want to build the images yourself, you can do it like this:

```
  $ docker build . -t koha/koha-dpkg:master --build-arg BRANCH=master
  $ docker build . -t koha/koha-dpkg:19.11  --build-arg BRANCH=19.11
  $ docker build . -t koha/koha-dpkg:19.05  --build-arg BRANCH=19.05
```

In order for the above commands to work, you need to have a _base.tgz_ image on
the root directory for the project.

### Using pre-made base.tgz files

Pre-made base.tgz files are maintained and kept up to date by _tcohen_ (on OFTC#koha).
You can get when by issuing the following command:


```
    export BRANCH=master
    wget https://theke-space-one.nyc3.digitaloceanspaces.com/base_${BRANCH}.tgz -O /base.tgz
```


### Use Pbuilder to build your own image

The base images are created using the following commands:

```
   sudo pbuilder create --distribution stretch --basetgz base.tgz
   sudo pbuilder login  --save-after-login     --basetgz base.tgz
```

Once the second command was run, the following commands were run inside the chroot:

* master

```
   echo "deb http://debian.koha-community.org/koha unstable main" > /etc/apt/sources.list.d/koha.list
   echo "deb [trusted=yes] http://apt.kohaaloha.com/koha-nightly dev main" >> /etc/apt/sources.list.d/koha.list
   apt install wget gnupg
   wget -O- http://debian.koha-community.org/koha/gpg.asc | apt-key add -
   apt update
   apt install koha-perldeps
   exit
```

* stable branches (notice we picked _19.11_, this is also valid for any stable branch that is available in the repo)

```
   export VERSION=18.05
   echo "deb http://debian.koha-community.org/koha $VERSION main" > /etc/apt/sources.list.d/koha.list
   apt install wget gnupg
   wget -O- http://debian.koha-community.org/koha/gpg.asc | apt-key add -
   apt update
   apt install koha-perldeps
   exit
```

## Troubleshooting

### unrepresentable changes to source
The Koha script that builds the packages doesn't accept the checkout to contain any file or dir that is not committed. Including files listed in `.gitignore`.

Example of directories that must be removed permanently or backed up elsewhere.

- node_modules
- koha-tmpl/intranet-tmpl/prog/css/maps/
- koha-tmpl/opac-tmpl/bootstrap/css/maps/
- .swp files
